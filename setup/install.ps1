function logAction ($msg) {
    Write-Host $msg -ForegroundColor Blue
}
function logError ($msg) {
    Write-Host $msg -ForegroundColor Red
}
function logInfo ($msg) {
    Write-Host $msg -ForegroundColor Yellow
}
function logSuccess ($msg) {
    Write-Host $msg -ForegroundColor Green
}
function logWarning ($msg) {
    Write-Host $msg -ForegroundColor Magenta
}
function CheckForChoco() {
    if (!(IsInstalled "choco")) {
        logWarning "Warning: choco is not installed"
        logAction "Start installing it"
        Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    }
}
function CheckForPsCore() {
    try {
        if ($null -eq $PSCORE) {
            logWarning "PowerShell core not installed. Starting installation process..."
        }
        $repo = "PowerShell/PowerShell"
        $releases = "https://api.github.com/repos/$repo/releases"
        $installedVersion = $PSVersionTable.PSVersion.ToString()
        $currentVersion = $PSVersionTable.PSVersion.ToString()
        $tag = "v$currentVersion"

        logAction "Determining latest release"
        $tag = (Invoke-WebRequest $releases -UseBasicParsing | ConvertFrom-Json)[0].tag_name
        $currentVersion = $tag.Substring(1)

        if ($installedVersion -eq $currentVersion) {
            Write-Host "PsCore is up to date: $installedVersion"
            return
        }
        Write-Host "New version available: Installed: $installedVersion, Available: $currentVersion"
        $msiName = "PowerShell-$currentVersion-win-x64.msi"
        $url = "https://github.com/$repo/releases/download/$tag/$msiName"
        $outPath = "c:\temp\$msiName"
        $MSIArguments = @(
            "/package $outPath"
            "/quiet ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1"
        )
        if (!(Test-Path -Path $outPath)) {
            Invoke-WebRequest $url -OutFile $outPath -UseBasicParsing
        }
        # /package $outPath /quiet ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1
        Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow
        Remove-Item -Path $outPath
        logInfo "Installation successfull, please restart the installation process"
        exit
    }
    catch {
        logError "Error: installing PowerShell Core failed: $PSItem.Exception.Message"
        exit
    }
}
function IsPackageInstalled($packageName) {
    if ((choco list --local-only | Select-String -Pattern $packageName)) {
        return $true
    }
    return $false
}
function InstallPackage($packageName) {
    choco install $packageName -Y
}
function CheckForNeoVim() {
    if (!(IsPackageInstalled "neovim")) {
        choco install neovim --pre -Y
    }

    $vimrcPath = "$ENV:USERPROFILE\AppData\Local\nvim"

    New-Item -ItemType Directory -Path (Join-Path -Path $vimrcPath -ChildPath "\autoload") -Force

    $uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    $outPath = Join-Path -Path $vimrcPath -ChildPath "\autoload\plug.vim"
    if (!(Test-Path -Path $outPath -PathType Leaf)) {
        Invoke-WebRequest $uri -OutFile $outPath -UseBasicParsing
    }
    Copy-Item "$TERMSOME\templates\_init.vim" "$vimrcPath\init.vim" -Force
}
function CheckForTermsome() {
    if (Test-Path -Path $TERMSOME) {
        logWarning "termsome is already installed.`nYou'll need to remove $TERMSOME in order to re-install it."
        exit
    }
    if (!(IsInstalled "git")) {
        logError "Error: git is not installed"
        Read-Host
        exit
    }

    $REPO = "https://quickstar@bitbucket.org/quickstar/termsome.git"
    logAction "Cloning termsome..." -ForegroundColor Blue
    try {
        # git clone --depth=1 $REPO "$TERMSOME"
        git clone $REPO "$TERMSOME"
    }
    catch {
        logError "Error: git clone of $REPO repo failed"
        Read-Host
        exit
    }
}
function IsInstalled($command) {
    if (Get-Command $command -ErrorAction SilentlyContinue) {
        return $true
    }
    return $false
}
function CheckForElevatedPriviledges () {  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    if (!(New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)) {
        logError "Error: This script requires elevated priviledges"
        exit
    }
}
function Load-Module ($m) {
    # If module is imported say that and do nothing
    if (Get-Module $m) {
        logInfo "Module $m is already imported"
    }
    else {
        # If module is not imported, but available on disk then import
        if (Get-Module -ListAvailable -Name $m) {
            Import-Module $m
            logSuccess "Import of module $m was successful"
        }
        else {
            # If module is not imported, not available on disk, but is in online gallery then install and import
            if (Find-Module -Name $m | Where-Object { $_.Name -eq $m }) {
                logInfo "Installing module $m"
                Install-Module -Name $m -Force -Verbose -Scope CurrentUser
                Import-Module $m
                logSuccess "Installation of module $m was successful"
            }
            else {
                # If module is not imported, not available and not in online gallery then abort
                logError "Module $m not imported, not available and not in online gallery, aborting installation...."
                EXIT 1
            }
        }
    }
}
function Restart() {
    logSuccess "Installation successful, restart your terminal session"
    logAction "by...."
    Start-Sleep -Seconds 1
    exit
}

$TERMSOME = ($ENV:USERPROFILE + "\.termsome")
$PSCORE = (Get-Item -Path "C:\Program Files\PowerShell\7*\pwsh.exe").FullName

#CheckForTermsome
CheckForElevatedPriviledges
#CheckForPsCore
#CheckForChoco
#CheckForNeoVim

Install-Module "posh-git" -Force -Scope AllUsers
Install-Module "oh-my-posh" -Force -Scope AllUsers
Install-Module -AllowClobber Get-ChildItemColor -Force -Scope AllUsers

$profilePath = $PROFILE
$profileFolderPath = Split-Path $PROFILE
$themePath = "$profileFolderPath\PoshThemes\"
$themeTemplatePath = "$TERMSOME\templates\PoshThemes\"

logInfo "Searching for PoshTheme templates at $themeTemplatePath"
if (!(Test-Path -Path $themePath)) {
    mkdir $themePath
}
Get-ChildItem -Path "$themeTemplatePath*" -Include *.psm1 -Recurse `
| Copy-Item -Destination $themePath -Force -PassThru `
| logSuccess "PoshTheme ${_.Name} installed"

logInfo "Looking for an existing PowerShell profile..."
if (Test-Path -Path $profilePath) {
    logInfo "Found existing profile: $profilePath, creating backup!"
    Move-Item $profilePath ($profilePath + ".pre-termsome") -Force
    logSuccess "Backup complete: $profilePath.pre-termsome"
}
logAction "Copying termsome PowerShell profile to $profilePath"
Copy-Item "$TERMSOME\templates\Microsoft.PowerShell_profile.ps1" $profileFolderPath -Force
