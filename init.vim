set nocompatible              " be iMproved, required
filetype off                  " required

call plug#begin('~/AppData/Local/nvim/plugged')
Plug 'morhetz/gruvbox'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'pprovost/vim-ps1'
call plug#end()

" Enable "true color" support in the terminal
set termguicolors
" let nvim now that we are using a dark terminal theme
set background=dark
" set gruvbox as our default theme
colorscheme gruvbox

set splitbelow
set splitright

set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber

" Enable autocompletion:
set wildmode=longest,list,full
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" YAML documents are required to have a 2 space indentation
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" number of visual spaces per TAB
set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab
" highlight current line
set cursorline
" highlight matching [{()}]
set showmatch

" To enable mode shapes, "Cursor" highlight, and blinking, check out ":help guicursor" for more options
set guicursor=n-v-c:block,i-ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175

" Terminals do not provide a way to query the cursor style. Use a VimLeave autocommand to set the cursor style when Nvim exits
au VimLeave * set guicursor=n-v-c:ver25,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
