# Installation

## Install with PowerShell.exe
To install with PowerShell, you must ensure Get-ExecutionPolicy is not Restricted. I suggest using Bypass to bypass the policy to get things installed or AllSigned for quite a bit more security.

Run `Get-ExecutionPolicy`. If it returns **Restricted**, then run `Set-ExecutionPolicy AllSigned` or `Set-ExecutionPolicy Bypass -Scope Process`.
Now run the following command:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/quickstar/termsome/raw/master/setup/install.ps1'))
```

> NOTE: Please inspect [install.ps1](https://bitbucket.org/quickstar/termsome/src/master/setup/install.ps1) prior to run the script to ensure safety.

# References
- [Pimping Up Your PowerShell & Cmder with Posh-Git, Oh-My-Posh, & Powerline Fonts](https://gist.github.com/jchandra74/5b0c94385175c7a8d1cb39bc5157365e)
- [posh-git](https://github.com/dahlbyk/posh-git)
- [oh-my-posh](https://github.com/JanDeDobbeleer/oh-my-posh)
- [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)